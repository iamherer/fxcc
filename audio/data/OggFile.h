#pragma once

#include "audio/core/pch.h"

#define STB_VORBIS_HEADER_ONLY
#include "stb_vorbis.c"

namespace Audio
{

    namespace Data
    {

        struct OggFile
        {
            std::string m_Path;

            stb_vorbis *vorbis;

            float *pcm;

            bool Init();

            OggFile(const std::string &path);

            void Free();

            virtual ~OggFile();
        };
    } 
};