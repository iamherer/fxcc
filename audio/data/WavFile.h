#pragma once

#include "audio/data/pch.h"

namespace Audio
{
    namespace Data
    {

        struct WavFile
        {

            struct Header
            {
                char riff[4];
                uint32_t chunkSize;
                char wave[4];
                char fmt[4];
                uint32_t subchunk1Size;
                uint16_t audioFormat;
                uint16_t numChannels;
                uint32_t sampleRate;
                uint32_t byteRate;
                uint16_t blockAlign;
                uint16_t bitsPerSample;
                char data[4];
                uint32_t dataSize;
            } m_Header;

            std::vector<char> m_Data;

            WavFile(const std::string &filename);

            bool LoadData(const std::string &filename);

            void FreeData();

            // int GetFormat() const;
        };
        
    };
};
