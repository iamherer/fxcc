#pragma once

#include "audio/core/pch.h"
#include "audio/core/Source.h"

namespace Audio
{
    namespace Core
    {
        struct Player
        {
          std::shared_ptr<Audio::Core::Source> RegisterOgg();
        };
    };
};