#pragma once

#include "audio/data/PCM.h"

namespace Audio
{
    namespace Core
    {
        struct Source
        {
            virtual int Create(const Audio::Data::PCM &pcm) = 0;

        };
    };
};