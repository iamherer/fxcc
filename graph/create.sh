
#/bin/bash

my_array=("dx9" "dx11" "dx12" "gl" "gl2" "gl3" "gl4" "gles" "gles2" "gles3" "metal" "vulkan")

func1()
{
    cp -r gl3 $1
}

for element in "${my_array[@]}"
do
    func1 $element
done
