#!/bin/bash

# 定义字符串数组
my_array=("dx9" "dx11" "dx12" "gl" "gl2" "gl3" "gl4" "gles" "gles2" "gles3" "metal" "vulkan")

func1()
{
    cd $1
        mkdir graph
        mv ./* graph/
    cd ..
}

for element in "${my_array[@]}"
do
    func1 $element
done
