#pragma once

#include <iostream>

#include <android/log.h>
#include <android_native_app_glue.h>
#include <android/asset_manager.h>
#include <EGL/egl.h>
#include <GLES3/gl3.h>

// #define LOGI(fmt, ...) __android_LOG_print(ANDROID_LOG_INFO, "info", fmt, ##__VA_ARGS__)
// #define LOGE(fmt, ...) __android_LOG_print(ANDROID_LOG_INFO, "error", fmt, ##__VA_ARGS__)
// #define LOGD(fmt, ...) __android_LOG_print(ANDROID_LOG_INFO, "debug", fmt, ##__VA_ARGS__)
// #define LOGW(fmt, ...) __android_LOG_print(ANDROID_LOG_INFO, "warning", fmt, ##__VA_ARGS__)
