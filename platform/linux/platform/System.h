#pragma once

#include "pch.h"

struct System
{
    struct Filter
    {
        std::string m_Name;
        std::vector<std::string> m_Patterns;
    };
    struct Filters
    {
        Filters()
        {
       
        };
        std::vector<Filter> m_Filters;

        std::string GenerateFilterString() const
        {
            std::ostringstream filterStream;

            for (const auto& filter : m_Filters)
            {
                filterStream << filter.m_Name << '\0';

                for (size_t i = 0; i < filter.m_Patterns.size(); ++i)
                {
                    filterStream << filter.m_Patterns[i];
                    if (i < filter.m_Patterns.size() - 1)
                    {
                        filterStream << ';';  
                    }
                }
                filterStream << '\0';  
            }

            return filterStream.str();
        }

    };

    static void Init(int argc, char *argv[]);

    static std::string OpenDialog(const std::string& title, const Filters& filters = Filters(), std::string submitBtn = "Open", std::string cancelBtn = "Cancel");

    static std::string SaveDialog(const std::string& title, const Filters& filters = Filters(), const std::string& defaultFilename = "Untitle", std::string submitBtn = "Save", std::string cancelBtn = "Cancel");
};