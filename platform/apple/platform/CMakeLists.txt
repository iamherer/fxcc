cmake_minimum_required(VERSION 3.15.0)

project(apple)

file(GLOB local_src *.cpp)
add_library(platform $${local_src})
