#include "System.h"
#include <Windows.h>

void System::Init(int argc, char *argv[])
{
}

std::string System::OpenDialog(const std::string &title, const Filters &filters, std::string submitBtn, std::string cancelBtn)
{
    OPENFILENAME ofn;       // Common dialog box structure
    char szFile[260] = {0}; // Buffer for file name

    // Initialize OPENFILENAME
    ZeroMemory(&ofn, sizeof(ofn));
    ofn.lStructSize = sizeof(ofn);
    ofn.hwndOwner = nullptr;
    ofn.lpstrFile = szFile;
    ofn.nMaxFile = sizeof(szFile);
    // ofn.lpstrFilter = "All Files\0*.*\0Text Files\0*.TXT\0";
    std::string lpstrFilter = filters.GenerateFilterString();
    // std::cout << lpstrFilter << std::endl;

    ofn.lpstrFilter = lpstrFilter.c_str();
    ofn.nFilterIndex = 1;
    ofn.lpstrFileTitle = nullptr;
    ofn.nMaxFileTitle = 0;
    ofn.lpstrInitialDir = nullptr;
    ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;

    // Display the Open dialog box
    if (GetOpenFileName(&ofn))
    {

        return ofn.lpstrFile;
    }
    return "";
}

std::string System::SaveDialog(const std::string &title, const Filters &filters, const std::string &defaultFilename, std::string submitBtn, std::string cancelBtn)
{
    OPENFILENAME ofn;       // Common dialog box structure
    char szFile[260] = {0}; // Buffer for file name
    strcpy(szFile, defaultFilename.c_str());

    // Initialize OPENFILENAME
    ZeroMemory(&ofn, sizeof(ofn));
    ofn.lStructSize = sizeof(ofn);
    ofn.hwndOwner = nullptr;
    ofn.lpstrFile = szFile;
    ofn.nMaxFile = sizeof(szFile);

    std::string lpstrFilter = filters.GenerateFilterString();
    std::cout << lpstrFilter << std::endl;

    ofn.lpstrFilter = lpstrFilter.c_str();
    ofn.lpstrFileTitle = nullptr;
    ofn.nMaxFileTitle = 0;
    ofn.lpstrInitialDir = nullptr;
    // ofn.lpstrDefExt = "txt";
    ofn.Flags = OFN_PATHMUSTEXIST | OFN_OVERWRITEPROMPT;

    // Display the Save dialog box
    if (GetSaveFileName(&ofn))
    {
        // User selected a file path to save
        // std::cout << "Save file: " << ofn.lpstrFile << std::endl;
        return ofn.lpstrFile;
    }
    return "";
}
