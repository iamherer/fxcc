#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include <istream>
#include <ostream>
#include <vector>
#include <map>
#include <unordered_map>
#include <memory>
#include <filesystem>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <future>
#include <chrono>

#pragma warning(disable : 4305)
#pragma warning(disable : 4005)
#pragma warning(disable : 4244)
