#pragma once

#include "pch.h"

namespace Ztclog
{
    struct Line
    {
        enum Type
        {
            _line_none_ = 0,
            _line_error_,
            _line_warn_,
            _line_info_,
            _line_debug_,
        } m_Type;
        std::string m_Text;
        Line() : m_Type(_line_info_)
        {
        }
    };
};