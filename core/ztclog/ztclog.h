#pragma once

#include "Cache.h"

namespace ztclog
{
    template <typename... Args>
    inline void debug(const char *format, Args &&...args)
    {
        Ztclog::Cache::Inst().Debug(format, std::forward<Args>(args)...);
    };

    template <typename... Args>
    inline void info(const char *format, Args &&...args)
    {
        Ztclog::Cache::Inst().Info(format, std::forward<Args>(args)...);
    };

    template <typename... Args>
    inline void warn(const char *format, Args &&...args)
    {
        Ztclog::Cache::Inst().Warn(format, std::forward<Args>(args)...);
    };

    template <typename... Args>
    inline void error(const char *format, Args &&...args)
    {
        Ztclog::Cache::Inst().Error(format, std::forward<Args>(args)...);
    };
}