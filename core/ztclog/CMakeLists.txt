cmake_minimum_required(VERSION 3.15.0)

project(stl)

set(CMAKE_CXX_STANDARD 17)

file(GLOB local_src *.cpp)

add_library(core-log ${local_src})
