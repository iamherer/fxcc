#pragma once

#include "core/graph/pch.h"

namespace Graph
{
    struct ImageFx
    {

        struct HD
        {

            int m_Width;
            int m_Height;
            int m_NrComponent;
            int m_Hdr;
            HD() : m_Width(0), m_Height(0), m_NrComponent(0), m_Hdr(false)
            {
            }

            int DataSize() const
            {
                return m_NrComponent * m_Width * m_Height;
            }

        } m_HD;

        void InitHD(int w, int h, int nr, int hdr);

        void *m_Data = 0;

        void AllocData();

        struct Desc
        {
            std::string m_Path;
            int m_hdr;
            int m_ReqComponent;
            Desc() : m_hdr(false), m_ReqComponent(0)
            {
            }
        } m_Desc;

        void Write(const std::string &path) const;

        void Write(std::ofstream &ofs) const;

        void Read(const std::string &path);

        void Read(std::ifstream &ifs);

        void WriteBMP(const std::string &path) const;

        void *GetData() const;

        void Free();

        void Resize(int w, int h);

        ImageFx() = default;

        ImageFx(const Desc &desc);

        bool m_Avail = false;

        bool Init();
    };
};