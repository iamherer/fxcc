#include "FileSystem.h"
#include "../ztclog/ztclog.h"

namespace fs = std::filesystem;

uint64_t Stl::FileSystem::FileSize(const std::string &source)
{
    return std::filesystem::file_size(source);
};

int Stl::FileSystem::CopyWrite(const std::string &source, const std::string &target, ProcessInfo &processInfo)
{
    if (!fs::exists(fs::path(source)))
    {
        ztclog::info("cannot find the file %s", source.c_str());
        return 0;
    }
    processInfo.m_Total = FileSize(source);

    fs::path _target(target);
    if (_target.has_parent_path())
    {
        fs::create_directories(_target.parent_path());
    }
    std::ifstream ifs(source, std::ios::binary);
    std::ofstream ofs(target, std::ios::binary);

    unsigned char buf[1024];

    while (ifs.read((char *)buf, sizeof(buf)))
    {
        int readLen = ifs.gcount();
        processInfo.m_Current += readLen;
        ofs.write((const char *)buf, readLen);
    }

    ofs.flush();
    ofs.close();
    ifs.close();

    return 0;
}
