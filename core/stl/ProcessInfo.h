
#pragma once

namespace Stl
{

    struct ProcessInfo
    {
        int m_Total;
        int m_Current;
        ProcessInfo() : m_Total(1), m_Current(0){}
    };
};