#pragma once

#include "pch.h"
#include "ProcessInfo.h"

namespace Stl
{
    struct FileSystem
    {
        static uint64_t FileSize(const std::string &source);
        static int CopyWrite(const std::string &source, const std::string &target, ProcessInfo &processInfo);
    };
}