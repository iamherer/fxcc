#pragma once

#include "core/stl/pch.h"
#include "../ztclog/ztclog.h"

namespace Stl
{

    struct Thread
    {
        std::shared_ptr<std::thread> m_Inst;

        enum State
        {
            _state_none_ = 0,
            _state_waiting_,
            _state_working_,
            _state_end_
        } m_State = _state_waiting_;

        bool End();

        bool Waiting();

        bool Working();

        void Start();

        static void Execute(Thread *inst);

        virtual void Run() = 0;

        void Join();

        std::thread::id GetId();

        void Detach();

        void Swap(std::shared_ptr<Thread> other);

        std::thread::native_handle_type NativeHandle();

        bool Joinable();

        void TryJoin();

        virtual ~Thread();
    };
}
